import { Client } from 'boardgame.io/client';
import { BattleTypesGame } from './Game';
import monsters from './monsters.json'
import { MCTSBot } from 'boardgame.io/ai';
import { Local } from 'boardgame.io/multiplayer';

const NUMBER_OF_CARDS_IN_HAND = 5;

class BattleTypesGameClient {
  constructor(rootElement) {
    this.client = Client(
        { game: BattleTypesGame,
        playerID: '0',
        multiplayer: Local({ bots: { '1': MCTSBot } }),
        debug: false,
        }
        );
    this.client.start();
    this.rootElement = rootElement;
    this.createBoard();
    this.attachListeners();
    this.client.subscribe(state => this.update(state));
  }

  createCardDisplay(name, linkindex) {
    const attacks_list = [`<td class="type"">A</td>`];
    const weakness_list = [`<td class="type"">W</td>`];
    for (let j = 0; j < 5; j++) {
        attacks_list.push(`<td class="type" id="${name}_attack_${j}"></td>`);
        weakness_list.push(`<td class="type" id="${name}_weakness_${j}"></td>`);
    }

    const rows = []
    rows.push(`<tr><td class="title" id="${name}_title"  colspan="6"></td></tr>`);
    rows.push(`<tr><td class="image" id="${name}_image"  colspan="6"></td></tr>`);
    rows.push(`<tr>${attacks_list.join('')}</tr>`);
    rows.push(`<tr>${weakness_list.join('')}</tr>`);
    if (linkindex > -1) 
    {
    return `<table class="player_click" data-id="${linkindex}">${rows.join('')}</table>`;
    }
    else
    { 
        return `<table>${rows.join('')}</table>`;
    }
    }

    createHandLine(index) {
        const card_list = []
        console.log(`cards: ${NUMBER_OF_CARDS_IN_HAND}`)
        for (let j = 0; j < NUMBER_OF_CARDS_IN_HAND; j++) {
            let inner = this.createCardDisplay(`p${index}_hand_${j}`, j);
            card_list.push(`<td class="card_parent" id="p${index}_hand_${j}" data-id="${j}">${inner}</td>`);
        }
        return `<tr>${card_list.join('')}</tr>`
    }

    createActive() {
        let inner = this.createCardDisplay(`shared_active`, -1);
        let card = `<td class="card_parent" id="shared_active">${inner}</td>`;
        
        return `<tr><td>The first player starts with playing any card of choice. From then on, each player playes a card where an attack symbol (A) matches at least one weakness symbol (W) of the previously played card. Each playes hand is always refilled to 5. First player without valid moves loses. </td><td>${card}</td><td></td></tr>`
    }

    createBoard() {
        let top = this.createHandLine(2)
        let mid = this.createActive()
        let bot = this.createHandLine(1)
        this.rootElement.innerHTML = `<table>${top}${mid}${bot}</table>`
    }

    updateCard(monster, name, markers, playertag) {
        if (monster === '') {
            this.rootElement.querySelector(`#${name}_title`).innerHTML = ""
            this.rootElement.querySelector(`#${name}_image`).innerHTML = ''
            for (let j = 0; j < 4; j++) {
                this.rootElement.querySelector(`#${name}_attack_${j}`).innerHTML = ''
                this.rootElement.querySelector(`#${name}_weakness_${j}`).innerHTML = ''
            };
        }
        else
        {
        this.rootElement.querySelector(`#${name}_title`).innerHTML = `<b><span class="${playertag}">${monsters[monster].title}</span></b>`
        let imgpath = monsters[monster].image
        this.rootElement.querySelector(`#${name}_image`).innerHTML = `<img class="card_m_img" src="img/in_use/${imgpath}" ></img>`
        for (let j = 0; j < 4; j++) {
            if (j < monsters[monster].attack.length) {
                let imgpath = monsters[monster].attack[j]
                if (markers.includes(imgpath))
                {
                    this.rootElement.querySelector(`#${name}_attack_${j}`).innerHTML = `<img class="card_type_img red_border" src="img/types/${imgpath}.png" title="${imgpath}"></img>`
                }
                else
                {
                    this.rootElement.querySelector(`#${name}_attack_${j}`).innerHTML = `<img class="card_type_img white_border" src="img/types/${imgpath}.png" title="${imgpath}"></img>`
                }
            }
            else
            {
                this.rootElement.querySelector(`#${name}_attack_${j}`).innerHTML = ''
            }
            if (j < monsters[monster].weakness.length) {
                let imgpath = monsters[monster].weakness[j]
                this.rootElement.querySelector(`#${name}_weakness_${j}`).innerHTML = `<img class="card_type_img" src="img/types/${imgpath}.png"  title="${imgpath}"></img>`
            }
            else
            {
                this.rootElement.querySelector(`#${name}_weakness_${j}`).innerHTML = ''
            }
        }
        }


    }

    attachListeners() {
        // This event handler will read the cell id from a cell’s
        // `data-id` attribute and make the `clickCell` move.
        const handleCellClick = event => {
            console.log(`move ${event.target.dataset.id}`);
          const id = parseInt(event.target.dataset.id);
          this.client.moves.playMonster(id);
        };
        // Attach the event listener to each of the board cells.
        const cells = this.rootElement.querySelectorAll('.player_click');
        cells.forEach(cell => {
          cell.onclick = event => {
            console.log(`move ${cell.dataset.id}`);
          const id = parseInt(event.target.dataset.id);
          this.client.moves.playMonster(cell.dataset.id);
        };
        });
      }

    update(state) {
        if (state.ctx.currentPlayer == 1) {
        this.updateCard(state.G.player1.active, "shared_active", [], "player1")
        }
        else
        {
        this.updateCard(state.G.player2.active, "shared_active", [], "player2")
        }

        for (let j = 0; j < NUMBER_OF_CARDS_IN_HAND; j++) {
            var markers1, markers2;
            if (state.G.player1.active != '') {
                markers1 = monsters[state.G.player1.active].weakness;
            } 
            else
            {
                markers1 = ''
            }
            
            if (state.G.player2.active != '') {
                markers2 = monsters[state.G.player2.active].weakness;
            } 
            else
            {
                markers2 = ''
            }
            this.updateCard(state.G.player1.hand[j], `p1_hand_${j}`, markers2, "player1")
            this.updateCard(state.G.player2.hand[j], `p2_hand_${j}`, markers1, "player2")
        }
    }


}


const appElement = document.getElementById('app');
const app2 = new BattleTypesGameClient(appElement);

