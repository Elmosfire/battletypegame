import { INVALID_MOVE } from 'boardgame.io/core';
import monsters from './monsters.json'

const NUMBER_OF_CARDS_IN_HAND = 5;

function player_empty()
{
    return {
        deck: [],
        hand: {
            0: '',
            1: '',
            2: '',
            3: '', 
            4: ''
        },
        handsize : NUMBER_OF_CARDS_IN_HAND,
        active: '',
        haswon:false
    };
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
 function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function init_player(deck) {
    let player = player_empty();
    player.deck = deck;
    shuffle(player.deck);
    draw_until_full(player);
    return player
}

function draw_until_full(player) {
    var i;
    for (i = 0; i < player.handsize; i++) {
        if (player.hand[i] === '') {
            player.hand[i] = player.deck.pop();
            if (player.hand[i] == undefined) {
                player.hand[i] = ''
            }
        }
    }
}

function play_card_by_index(player, index) {
    player.active = player.hand[index];
    player.hand[index] = '';
}

function findCommonElements(arr1, arr2) {
    return arr1.some(item => arr2.includes(item))
}

function is_reply_valid(defender, attacker) {
    return findCommonElements(monsters[defender].weakness, monsters[attacker].attack)
}


function symmetric_state(G, ctx) {
    let v = {}
    v.attacker = [G.player1, G.player2][ctx.currentPlayer]
    v.defender = [G.player2, G.player1][ctx.currentPlayer]
    return v

}

function inverse_symmetric_state(G, ctx) {
    let v = {}
    v.attacker = [G.player2, G.player1][ctx.currentPlayer]
    v.defender = [G.player1, G.player2][ctx.currentPlayer]
    return v

}

function can_play_hand_by_index(symstate, index) {
    if (symstate.attacker.hand[index] == '') {
        return false
    }
    return (symstate.defender.active === '') || (is_reply_valid(symstate.defender.active, symstate.attacker.hand[index]))
}



function globmove(G, ctx, index) {
    let symstate = symmetric_state(G, ctx);
    if (can_play_hand_by_index(symstate, index)) {
        play_card_by_index( symstate.attacker, index);
        draw_until_full(symstate.attacker);
    }
    else
    {
        console.log("INVALID MOVE")
        return INVALID_MOVE
    }
}

function is_valid_move_given_state(G, ctx, index) {
    let symstate = symmetric_state(G, ctx);
    return can_play_hand_by_index(symstate, index);
}

function NoValidMovesForAttacker(symstate) {
    for (let i = 0; i < symstate.attacker.handsize; i++) {
        if (can_play_hand_by_index(symstate, i))  {
            console.log(`valid move ${i}, game is still running`)
            return false
        }
    }
    console.log(`no valid moves game is done`)
    return true
}



const startdeck = Object.keys( monsters )


export const BattleTypesGame = {
    setup: () => ({ player1: init_player([...startdeck]), player2: init_player([...startdeck]) }),
    moves: {
      playMonster: globmove
    },
    ai: {
        enumerate: (G, ctx) => {
          let moves = [];
          for (let i = 0; i < 5; i++) {
            if (is_valid_move_given_state(G, ctx, i)) {
              
              moves.push({ move: 'playMonster', args: [i] });
            }
          }
          return moves;
        },
      },
    endIf: (G, ctx) => {
        if (G.player1.haswon) {
            return {winner: 0}
        }
        if (G.player2.haswon) {
            return {winner: 1}
        }
      },

      turn: {
        minMoves: 1,
        maxMoves: 1,
    
        // Called at the beginning of a turn.
        onBegin: (G, ctx) => {
            let symstate = symmetric_state(G, ctx);
            console.log("ON BEGIN")
            if (NoValidMovesForAttacker(symstate)) {
                symstate.defender.haswon = true;
            }
        }
      }
}

