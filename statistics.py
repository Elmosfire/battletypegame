import json
import shutil
import os
from collections import Counter


with open("src/monsters.json") as file:
    data = json.load(file)
    
attack = Counter()
weakness = Counter()
    
for x in data.values():
    for y in x["attack"]:
        attack[y] += 1
    for y in x["weakness"]:
        weakness[y] += 1
        
        
for key in set(attack) | set(weakness):
    
    print(f"{key:<12}: {attack[key]:02} {weakness[key]:02}")
    
b_attack = attack-weakness
b_weakness = weakness-attack
print("attack: ", b_weakness.most_common()[:3])
print("weakness: ", b_attack.most_common()[:3])