import json
import shutil
import os

shutil.rmtree("public")
shutil.copytree("dist", "public")

with open("public/index.html") as file:
    data = ''.join(file.readlines())
    
data = data.replace("<script src=\"/App","<script src=\"App")
    
with open("public/index.html", 'w') as file:
    file.write(data)

os.mkdir(f"public/img/in_use")
with open("src/monsters.json") as file:
    for x in json.load(file).values():
        img = x["image"]
        shutil.copy(f"monsters/{img}", f"public/img/in_use/{img}")